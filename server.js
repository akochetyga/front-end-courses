'use strict';

const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const cors = require('cors');

mongoose.connect('mongodb://localhost/fec');

const db = mongoose.connection;

db.on('error', console.error.bind(console, 'connection error:'));

db.once('open', function() {
  console.log('Connected to MongoDB.');
});

const Schema = mongoose.Schema;

const commentSchema = new Schema({
  page: {type: String, required: true},
  name: String,
  email: String,
  message: String,
  date: {type: Date, default: Date.now}
});

const audioSchema = new Schema({
  page: {type: String, required: true},
  url: String,
  title: String
});

const videoSchema = new Schema({
  page: {type: String, required: true},
  url: String,
  title: String
});

const Comment = mongoose.model('Comment', commentSchema);
const Audio = mongoose.model('Audio', audioSchema);
const Video = mongoose.model('Video', videoSchema);

app.use(express.static(__dirname + '/public'));
app.use(bodyParser.json());
app.use(cors());

// Comments API.
app.get('/:namespace/comment', function(req, res) {
  console.log('Comments list...');
  Comment.find({page: req.params.namespace}, function(err, data) {
    res.json(data);
  });
});

app.put('/:namespace/comment', function(req, res) {
  req.body.page = req.params.namespace;

  let comment = new Comment(req.body);

  comment.save(function(err) {
    if (err) {
      res.status(400).json({status: 'error', error: err.message});
    } else {
      res.json({status: 'ok'});
    }
  });
});

app.delete('/:namespace/comment/:id', function(req, res) {
});

// Audio API.
app.get('/:namespace/audio', function(req, res) {
  console.log('Audio list...');
  Audio.find({page: req.params.namespace}, function(err, data) {
    res.json(data);
  });
});

app.put('/:namespace/audio', function(req, res) {
  req.body.page = req.params.namespace;

  let audio = new Audio(req.body);

  audio.save(function(err) {
    if (err) {
      res.status(400).json({status: 'error', error: err.message});
    } else {
      res.json({status: 'ok'});
    }
  });
});

app.delete('/:namespace/audio/:id', function(req, res) {
});

// Video API.
app.get('/:namespace/video', function(req, res) {
  console.log('Video list...');
  Video.find({page: req.params.namespace}, function(err, data) {
    res.json(data);
  });
});

app.put('/:namespace/video', function(req, res) {
  req.body.page = req.params.namespace;

  let video = new Video(req.body);

  video.save(function(err) {
    if (err) {
      res.status(400).json({status: 'error', error: err.message});
    } else {
      res.json({status: 'ok'});
    }
  });
});

app.delete('/:namespace/video/:id', function(req, res) {
});


app.listen(8888, function () {
  console.log('front-end-course app listening on port 8080!');
})
