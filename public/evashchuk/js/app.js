

var myPage = {
  pageTitle: 'Evgeniy Vashchuk',
  name: 'Evgeniy Vashchuk',
  city: 'Kyiv',
  vk: 'http://vk.com/evgeniy_vashchuk',
  fb: 'https://www.facebook.com/evgeniy.vashchuk.1',

  setValue: function (value, elemenytId, prop) {
    var someElement = document.getElementById(elemenytId);
    someElement.innerHTML = value;
    this[prop] = value;
  },
  getValue: function (prop) {
    return this[prop];
  },
  setLinkAdrress: function (newAdrress, elemenytId, prop) {
    var someElement = document.getElementById(elemenytId);
    someElement.setAttribute('href', newAdrress);
    this[prop] = value;
  }
}
