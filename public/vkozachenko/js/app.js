 function MainUserPage(args) {
   this.title = args.title;
   this.userName = args.userName;
   this.userProfession = args.userProfession;
   this.userPhone = args.userPhone;
   this.userEmail = args.userEmail;
   this.userAddress = args.userAddress;

   for(var iter in args) {
     if(!args.hasOwnProperty(iter)) continue;
     var node = document.getElementById(iter);
     if(node) {
       node.innerHTML = args[iter];
     }
   }

   this.createText = function (id, text) {
     var node = document.getElementById(id),
         container = document.createElement('p');
         node.appendChild(container);

     if (node) {
       container.innerHTML = text;
     }
   };
   this.createList = function (id, listArray, addClassName) {
     var node = document.getElementById(id),
         ul = document.createElement('ul');
     ul.className = addClassName;
     node.appendChild(ul);
     for (var i = 0; i < listArray.length; i++) {
       var li = document.createElement('li');
       li.innerHTML = listArray[i];
       ul.appendChild(li);
     }
   };
   this.setContent = function(id, text) {
     var node = document.getElementById(id);
     if(node) {
       node.innerHTML = text;
     }
   };
 }

 var page = new MainUserPage ({
   title: 'Vitoria Kozachenko',
   userName:'Vitoria<br>Kozachenko',
   userProfession:'front-end developer',
   userPhone: '+38(050)-710-84-65',
   userEmail: 'viktoria.koza4enko.v@gmail.com',
   userAddress: 'Kiev, Geroev Dnepra str.'
 });

 var biographyText = 'I\'m 25 years old front-end developer from Kiev. I don\'t have experience yet. I love to travel, read books and have time with my friends.',
     educationList = ['Admiral Makarov National University of Shipbuilding<br>shipbuilding fuculty "Ships and Ocean Technology"','Prog.kiev.ua<br>"Java"','PHP Academy<br>"Frontend"'],
     experienceList = ['Here I will place my experience','Here I will place my experience','Here I will place my experience'],
     languages = ['Ukrainian', 'Russian', 'English'],
     userReferences = ['References', 'References', 'References'],
     userSkills = ['HTML', 'CSS', 'Javascript', 'SASS', 'Git'];

 page.createText('biography', biographyText);
 page.createList('education', educationList, 'list-style-image');
 page.createList('experience', experienceList, 'list-style-image');
 page.createList('languageSkills', languages);
 page.createList('references', userReferences);
 page.createList('professionalSkills', userSkills);