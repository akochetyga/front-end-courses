# Friends-module v1.0.0
---

## What is friends-module?

Friend-module allows to add and remove friends in Hackmates community.

## Dependencies

* AngularJS

## Usage

Require Friends-module in your JS file with argument, that represents your id.

    require('./friends-module.js')('yourId');

In your HTML document you should add:

    <div class="friends-module"></div>

##Users ID

* Alexander Kochetyga  574d93407cfab0a5527e8cc9
* Evgeniy Vashchuk  574d93b77cfab0a5527e8cca
* Ivan Nesenuk 574ed3a2627d622a5520e85a
* Vladimir Fedianin 574ed41e627d622a5520e85b
* Viktoria Kozachenko 574ed436627d622a5520e85c
* Maria Ponomarets 574ed492627d622a5520e85d
* Mikhail Haryst 574ed55e627d622a5520e85e
* Dmytro Nalyvaiko 57500c2a627d622a5520e85f
