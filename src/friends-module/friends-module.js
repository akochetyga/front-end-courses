/*Friends module v1.0.0*/

module.exports = function(userId) {
  var container = document.querySelector('.friends-module');

  var styleSheet = `
    .friends-module * {
      margin: 0;
      padding: 0;
      font-family: sans-serif;
      color: #777;
      box-sizing: border-box;
    }

    .friends-module {
      text-align: center;
    }

    .friends-module .friends-list{
      margin: 20px 0 50px;
    }

    .friends-module .friends-list > h1,
    .friends-module .users-list > h1 {
      text-transform: uppercase;
      margin-bottom: 10px;
      color: #555;
    }

    .friends-module button{
      text-transform: uppercase;
      font-weight: bold;
      font-size: 12px;
      margin-top: 10px;
      color: white;
      width: 128px;
      height: 33px;
      border: 0;
      cursor: pointer;
    }

    .friends-module .friends-list button{
      background-color: #FFC258;
    }

    .friends-module .users-list button{
      background-color: #69f777;
    }

    .friends-module ul {
      list-style: none;
    }

    .friends-module ul li{
      display: inline-block;
      margin: 5px;
    }

    .friends-module ul li h1{
      font-size: 20px;
      margin-bottom: 10px;
    }

    .friends-module article {
      border: 1px solid #777;
      text-align: center;
      width: 150px;
      height: 150px;
      padding: 10px;
      background-color: #fff;
    }

    .friends-module article a {
      text-decoration: none;
      color: #4f4f4f;
      font-size: 11px;
    }

  `;

  var styleElem = document.createElement('style');
  styleElem.innerHTML = styleSheet;
  document.head.appendChild(styleElem);


  container.innerHTML = `
    <div ng-controller="friendsController">
      <div class="friends-list">
        <h1>Friends List</h1>
        <label for="friendsSearch">Type name to find:</label>
        <input type="text" ng-model="searchFriend" id="friendsSearch">
        <ul>
          <li ng-repeat="friend in friends | filter: searchFriend">
            <article>
              <h1>{{friend.name}}</h1>
              <a href="mailto:{{friend.email}}">{{friend.email}}</a>
              <br>
              <button ng-click="deleteFriend(friend)">Delete from friends</button>
            </article>
          </li>
        </ul>
      </div>
      <div class="users-list">
        <h1>Possible Friends List</h1>
        <label for="usersSearch">Type name to find:</label>
        <input type="text" ng-model="searchUser" id="usersSearch">
        <ul>
          <li ng-repeat="user in users | filter: searchUser" ng-if="!(user.pageOwner || user.isFriend)">
            <article>
              <h1>{{user.name}}</h1>
              <a href="mailto:{{user.email}}">{{user.email}}</a>
              <br>
              <button ng-click="addFriend(user)">Add to friends</button>
            </article>
          </li>
        </ul>
      </div>
    </div>
  `;

  var friends = angular.module('friends', []);

  friends.service('FriendsService', ['$http', '$q', function($http, $q) {
    var usersList = 'http://52.29.149.132:8888/user';
    var friendsList = 'http://52.29.149.132:8888/user/' + userId + '/friend';

    this.getUsers = function() {
      return ($q.all([
        $http.get(usersList),
        $http.get(friendsList)
      ]).then(function(response) {
        var friends;
        for (var z = 0; z < response[0].data.length; z++) {
          if (response[0].data[z]._id === userId) {
            response[0].data[z].pageOwner = true;
            friends = response[0].data[z].friends;
            break;
          }
        }

        for (var i = 0; i < response[0].data.length; i++) {
          for (var j = 0; j < friends.length; j++) {
            if (response[0].data[i]._id === friends[j]) {
              response[0].data[i].isFriend = true;
            } else if (response[0].data[i]._id !== friends[j]
                    && !response[0].data[i].isFriend
                    && !response[0].data[i].pageOwner) {
              response[0].data[i].isFriend = false;
            }
          }
        }

        return ([response[0].data, response[1].data]);
      }));
    };

    this.addFriend = function(user) {
      return ($http.put(friendsList + '/' + user));
    };

    this.deleteFriend = function(friend) {
      return ($http.delete(friendsList + '/' + friend));
    };
  }]);

  friends.controller('friendsController', ['$scope', 'FriendsService', function($scope, FriendsService) {
    FriendsService.getUsers().then(function(response) {
      $scope.users = response[0];
      $scope.friends = response[1];
    });

    $scope.deleteFriend = function(friend) {
      FriendsService.deleteFriend(friend._id).then(function() {
        $scope.friends.splice($scope.friends.indexOf(friend), 1);
        for (var i = 0; i < $scope.users.length; i++) {
          if (friend._id === $scope.users[i]._id) {
            $scope.users[i].isFriend = false;
          }
        }
      });
    };

    $scope.addFriend = function(user) {
      FriendsService.addFriend(user._id).then(function() {
        user.isFriend = true;
        $scope.friends.push(user);
      });
    };

  }]);
};
