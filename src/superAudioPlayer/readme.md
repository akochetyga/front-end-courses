Super-audio-player
---------------
A simple module for playing audio files. 

Version Nubbers
---------------

5/06/2016 (v0.0.1) 

Getting Started
---------------
 
* Add this code in that part of the page where you would like to display a player.
        
<div ng-app="teslaAudioPlayer" class="audio-player-tesla-team" audio-player-tesla></div>

* Make sure that you include all scss and js file that you need.    
 
* Make sure that you gave the correct address (urlServer).
        
Example: var clientAudioPlayer = new audioPlayer(urlServer);
