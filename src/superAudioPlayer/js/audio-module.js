﻿function audioPlayer(urlServer) {
  var googleIcon = document.createElement('link');
  googleIcon.rel = 'stylesheet';
  googleIcon.href = 'https://fonts.googleapis.com/icon?family=Material+Icons';
  document.getElementsByTagName('head')[0].appendChild(googleIcon);
  angular.module('teslaAudioPlayer', []).directive('audioPlayerTesla', function () {
    return {
      restrict: 'A',
      controller: function ($scope, $http) {
        $scope.player = document.getElementById('player');
        $scope.attrLoop = false;
        $scope.content = [];
        $scope.titleTrack = "Some track";
        $scope.icons = ["skip_previous", "skip_next", "loop", "repeat", "shuffle", "add"];
        $scope.titleInForm = "";
        $scope.urlInForm = "";

        $scope.getContent = function () {
          $http.get(urlServer)
        .then(function (response) {
          $scope.content = response.data;
          $scope.titleTrack = $scope.content[0].title;
          $scope.player.src = $scope.content[0].url;
        }, function (response) {
          console.log('Houston we have a problem' + ' statuscode:' + response.status + ', statustext:' + response.statustext)
        });
        };

        $scope.getContent();

        $scope.deleteTrack = function (elemId) {
          $http.delete(urlServer + '/' + elemId)
          .then(function (response) {
            $scope.getContent();
          }, function (response) {
            console.log('Houston we have a problem' + ' statuscode:' + response.status + ', statustext:' + response.statustext)
          })
        };
      },
      link: function ($scope, $element, $attrs) {
        var player = document.getElementById('player');

        $scope.clickOnButton = function () {
          var action = (event.currentTarget.innerHTML);
          switch (action) {
            case $scope.icons[0]:
              $scope.playPreviousTrack($scope.player.src);
              break;
            case $scope.icons[1]:
              $scope.playNextTrack();
              break;
            case $scope.icons[2]:
              $scope.playLoopTrack();
              break;
            case $scope.icons[3]:
              $scope.playListLoop();
              break;
            case $scope.icons[4]:
              $scope.playListRandom();
              break;
            case $scope.icons[5]:
              $scope.showHideForm();
              break;
          }
        };

        $scope.submit = function () {
          var data = {
            title: $scope.titleInForm,
            url: $scope.urlInForm
          }
          $scope.titleInForm = "";
          $scope.urlInForm = "";
          $http.put('urlServer', data).then(function (response) {
            $scope.getContent();
          }, function (response) {
            console.log('Houston we have a problem' + ' statuscode:' + response.status + ', statustext:' + response.statustext)
          })
        };

        $scope.playInPlayer = function (title, url) {
          $scope.player.pause();
          $scope.player.currentTime = 0;
          $scope.titleTrack = title;
          $scope.player.src = url;
          $scope.player.play();
          if ($scope.$root.$$phase == '$apply' || $scope.$root.$$phase == '$digest') {
          }
          else {
            $scope.$apply();
          }
        };

        $scope.showHideForm = function () {
          if (document.getElementById('form-container').classList.contains('hide-elem') === true) {
            document.getElementById('form-container').className = 'show-elem';
            document.querySelector('[data-id="add"]').className += ' selected-button';
          }
          else {
            document.getElementById('form-container').className = 'hide-elem';
            document.querySelector('[data-id="add"]').classList.remove('selected-button');
          }
        };

        $scope.playLoopTrack = function () {
          $scope.checkClassOnButton([document.querySelector('[data-id="shuffle"]'), document.querySelector('[data-id="repeat"]')]);
          if (player.hasAttribute('loop') === false) {
            player.setAttribute('loop', 'loop');
            player.play();
            document.querySelector('[data-id="loop"]').className += ' selected-button';

          }
          else {
            player.removeAttribute('loop', 'loop');
            document.querySelector('[data-id="loop"]').classList.remove('selected-button');
          }
        };

        $scope.playNextTrack = function () {
          for (var i = 0; i < $scope.content.length; i++) {
            if ($scope.player.src === $scope.content[i].url && i < $scope.content.length - 1) {
              $scope.playInPlayer($scope.content[i + 1].title, $scope.content[i + 1].url);
              break;
            }
            else {
              if ($scope.player.src === $scope.content[i].url) {
                $scope.playInPlayer($scope.content[0].title, $scope.content[0].url);
                break;
              }
            }
          }
        };

        $scope.playPreviousTrack = function (url) {
          for (var i = 0; i < $scope.content.length; i++) {
            if (url === $scope.content[i].url && i > 0) {
              $scope.playInPlayer($scope.content[i - 1].title, $scope.content[i - 1].url);
              break;
            }
            else {
              if (url === $scope.content[i].url) {
                $scope.playInPlayer($scope.content[$scope.content.length - 1].title, $scope.content[$scope.content.length - 1].url);
                break;
              }
            }
          }
        };

        $scope.playListRandom = function () {
          $scope.checkClassOnButton([document.querySelector('[data-id="repeat"]'), document.querySelector('[data-id="repeat"]')]);
          if (document.querySelector('[data-id="shuffle"]').classList.contains('selected-button') === false) {
            document.querySelector('[data-id="shuffle"]').className += ' selected-button';
            player.addEventListener('ended', $scope.playRandom);
            player.removeEventListener('ended', $scope.playNextTrack);

          }
          else {
            document.querySelector('[data-id="shuffle"]').classList.remove('selected-button');
            player.removeEventListener('ended', $scope.playRandom);
          }
        };

        $scope.playRandom = function () {
          var randomNum = Math.floor(Math.random() * $scope.content.length);
          if (player.hasAttribute('loop') === true) {
            player.removeAttribute('loop', 'loop');
          }
          if ($scope.player.src === $scope.content[randomNum].url) {
            $scope.playNextTrack();
          }
          else {
            $scope.playInPlayer($scope.content[randomNum].title, $scope.content[randomNum].url);
          };

        };

        $scope.playListLoop = function () {
          $scope.checkClassOnButton([document.querySelector('[data-id="shuffle"]'), document.querySelector('[data-id="loop"]')]);
          if (player.hasAttribute('loop') === true) {
            player.removeAttribute('loop', 'loop');
          }
          if (document.querySelector('[data-id="repeat"]').classList.contains('selected-button') === false) {
            document.querySelector('[data-id="repeat"]').className += ' selected-button';
            player.addEventListener('ended', $scope.playNextTrack);
            player.removeEventListener('ended', $scope.playRandom);

          }
          else {
            document.querySelector('[data-id="repeat"]').classList.remove('selected-button');
            player.removeEventListener('ended', $scope.playNextTrack);
          }
        };

        $scope.checkClassOnButton = function (arr) {
          for (var i = 0; i < arr.length; i++) {
            if (arr[i].classList.contains('selected-button')) {
              arr[i].classList.remove('selected-button');
            }
          }
        };
      },
      template: [
      '<p class="audio-player-song-name">{{titleTrack}}</p>\
        <audio controls src="http://htmlka.com/wp-content/uploads/2009/06/08-caravan.mp3" id="player">Can not play</audio>\
        <div class="controls">\
        <span class="material-icons icons-color" ng-repeat="i in icons" ng-click="clickOnButton($event)" data-id={{i}}>{{i}}</span>\
        </div>\
        <span>Audio playlist</span>\
        <div class="audio-playlist">\
        <ul><li class="elem-playlist" ng-repeat="elem in content" data-url={{elem.url}} ng-click="playInPlayer(elem.title,elem.url)" >{{elem.title}}\
        <span class="material-icons icons-color hide-elem" ng-click="deleteTrack(elem._id)">clear</span></li><ul>\
        </div>\
        <div class="hide-elem" id="form-container">\
        <h3>Input title and url track</h3>\
        <form name="addTrack" ng-submit="submit()">\
        <input placeholder="Input title track" class="form-field" required ng-model="titleInForm"/>\
        <input placeholder="Input url track"class="form-field" required ng-model="urlInForm"/>\
        <button type="submit" class="button-submit">Add track</button>\
        </form>\
        </div>'
      ],
    }
  });
}

module.exports = audioPlayer;