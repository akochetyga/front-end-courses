# Video-player v1.0.0
---------------------

## What is video-player?

Video-player allows you to watch video on your page.

## Usage

Plug in Video-player in your JS file with argument (id of the tag where video player will be placed).

    var userPlayer = require('./video-player.js');
    var player = new userPlayer('your_id');

In your HTML document you should add:

  in tag <head>:
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

  in tag <body>:
   <div id="your_id" class="player-wrapper">

  in directory with .scss files copy file player-styles.scss

  in file main.scss:
    @import "player-styles";