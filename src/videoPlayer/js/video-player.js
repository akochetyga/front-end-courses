/**
 * Video Player
 * @param {string} id - The id of the tag where video player will be placed
 * @constructor
 */
function VideoPlayer(id) {

  var playerTemplate =
    `<article class="main-video-player-wrapper">
      <h2>Video</h2>
      <%=button%>
      <div class="main-video-wrapper">
        <div class="video-player">
          <video data-id="videoPlayer" src="<%=src%>" width="500px"></video>
          <p data-id="title"><%=videoTitle%></p>
          <div class="controls" data-id="controls">
            <input type="range" data-id="progressBar" class="input-range" value="0" text="0% played">
            <div data-id="videoButtons"></div>
            <input type="range" data-id="volumeBar" class="input-range-volume" value="0.4" min="0" max="1" step="0.1">
          </div>
        </div>
        <div class="video-play-list" data-id="videoPlayList"></div>
      </div>
    </article>`;
  var buttonTemplate =
    `<button data-id="<%=dataId%>">
      <i class="material-icons md"><%=iconImage%></i>
      <%=text%>
    </button>`;
  var buttonsContent = {
    play: 'play_arrow',
    previous: 'skip_previous',
    next: 'skip_next',
    repeatOne: 'repeat_one',
    shuffle: 'shuffle',
    repeat: 'repeat',
    volume: 'volume_up'
  };
  /**
   * Object with links on buttons
   * to access the button - buttonsLinks.play.addEventListener(...)
   * @type {object}
   */
  var buttonsLinks = {
    play: '',
    previous: '',
    next: '',
    repeatOne: '',
    shuffle: '',
    repeat: '',
    volume: ''
  };
  var userPlayerID = document.getElementById(id),
    /**
     * @example
     * objectWithItems{
     *   0: {
     *      title: video_title,
     *      source: video_source,
     *      button: button_for_start_playing_this_video_from_playlist
     *      }
     * }
     */
      objectWithItems = {},
      isLoopVideoEnable = false,
      isLoopPlayListEnable = false,
      isRandomEnabled = false,
      itemSource = 0,
      controlsWrapper,
      video,
      playerContext,
      progressBar,
      itemTitle;
  /**
   * Need to replace this with real data
   * @type {[]}
   */
  var dataFromServer = [
    {
      "_id":"5718a9f0fad1288369b30ec1",
      "url":"http://ak2.picdn.net/shutterstock/videos/3595802/preview/stock-footage-beautiful-young-woman-smiling-while-surfing-the-net-at-beach.mp4",
      "title":"Relaxing sunset"
    },
    {
      "_id":"5717908cfad1288369b30ec0",
      "url":"http://www.sample-videos.com/video/mp4/720/big_buck_bunny_720p_50mb.mp4",
      "title":"Big Buck Bunny"
    },
    {
      "_id":"5718a9f0fad1288369b30e56",
      "url":"http://www.amcwarpie.vipserv.org/test_buffer/perigny.mp4",
      "title":"Relaxing sunset"
    }

  ];

  function setLoopVideoEnable(loopVideoEnable) {
    isLoopVideoEnable = loopVideoEnable
  }

  function createObjectWithItems (arr){
      for(var i=0; i<arr.length; i++){

        if(arr[i].url == undefined) continue;

        var button = document.createElement('button');

        objectWithItems[i] = {
         title:arr[i].title,
         source:arr[i].url,
         button:button
       };
     }
  }

  function createButtons() {
    for (var key in buttonsContent) {

      if (!buttonsContent.hasOwnProperty(key)) continue;

      var buttonContext = {
        dataId: [key],
        iconImage: buttonsContent[key],
        text: ''
      };

      var buttonsWrapper = document.createElement('div');
      buttonsWrapper.innerHTML = generateTemplate(buttonTemplate, buttonContext);
      buttonsLinks[key] = buttonsWrapper.firstChild;
      controlsWrapper.appendChild(buttonsLinks[key]);
    }
  }

  function doActiveButton(isTrue, button) {
    switch (isTrue) {
      case true:
        button.className = 'active';
        break;
      case false:
        button.className = '';
        break;
    }
  }

  function changeButtonContent (button, currentContent, content) {
    var icon = button.children[0];

    if (icon.textContent == currentContent){
      icon.textContent = content;
    } else {
      icon.textContent = currentContent;
    }
  }

  function updateProgressBar() {
    var percentage = Math.floor((100 / video.duration) * video.currentTime);
    progressBar.value = percentage;
    progressBar.innerHTML = percentage + '% played';
  }

  function changeProgress(){
    video.removeEventListener("timeupdate", updateProgressBar);
    video.currentTime = Math.floor(video.duration * (progressBar.value / 100));
    video.addEventListener("timeupdate", updateProgressBar);
  }

  function loopVideo() {
    video.loop = isLoopVideoEnable;
  }

  function playPause() {

    if(video.paused) {
      video.play();
    } else {
      video.pause();
    }
  }

  function playMusic(songIndex){
    var source = objectWithItems[songIndex].source;
    video.setAttribute('src', source);
    video.play();
    itemSource = songIndex;
    itemTitle.textContent = objectWithItems[itemSource].title;
  }

  function playNextItem(event){
    event.preventDefault();
    event.stopImmediatePropagation();
    playMusic(getNextItemId('next'));
  }

  function playPreviousItem(event){
    event.preventDefault();
    event.stopImmediatePropagation();
    playMusic(getNextItemId('previous'))
  }

  function getNextItemId(isPreviousOrNext){

    if(isLoopVideoEnable){
      isLoopPlayListEnable = isRandomEnabled = false;
      return itemSource;
    }
    var nextId;

    switch (isPreviousOrNext){

      case 'next':
        nextId = itemSource+1;

        if (nextId >= Object.keys(objectWithItems).length){
          nextId = Object.keys(objectWithItems).length - 1;
        }
        break;

      case 'previous':
        nextId = itemSource-1;

        if (nextId < 0){
          nextId = 0;
        }
        break;
    }
    itemSource = nextId;
    return nextId;
  }

  /**
   * Initialization of templates, variables and functions
   */

  createObjectWithItems(dataFromServer);

  playerContext = {
    videoTitle: objectWithItems[0].title,
    src: objectWithItems[0].source,
    button: generateTemplate(buttonTemplate, {dataId:'add-video', iconImage:'add', text: 'Add video'})
  };

  userPlayerID.innerHTML = generateTemplate(playerTemplate, playerContext);
  controlsWrapper = document.querySelector('[data-id="videoButtons"]');
  video = document.querySelector('[data-id="videoPlayer"]');
  progressBar = document.querySelector('[data-id="progressBar"]');
  itemTitle = document.querySelector('[data-id="title"]');

  createButtons();

  /**
   * Event handlers
   */

  function loopVideoHandler(event) {
    event.preventDefault();
    setLoopVideoEnable(!isLoopVideoEnable);
    doActiveButton(isLoopVideoEnable, this);
    loopVideo();
  }

  function playPauseHandler(event) {
    event.preventDefault();
    playPause();
    changeButtonContent(this, 'play_arrow', 'pause')
  }

  function changeProgressHandler(event) {
    event.preventDefault();
    changeProgress();
  }

  /**
   * Event listeners
   */

  video.addEventListener("timeupdate", updateProgressBar);
  progressBar.addEventListener("change", changeProgressHandler);
  buttonsLinks.repeatOne.addEventListener('click', loopVideoHandler);
  buttonsLinks.play.addEventListener('click', playPauseHandler);
  buttonsLinks.previous.addEventListener('click', playPreviousItem);
  buttonsLinks.next.addEventListener('click', playNextItem);

  //==============================================//
  // Simple JavaScript Templating
  // John Resig – http://ejohn.org/ – MIT Licensed

  /**
   * Template function
   * @param {string} str
   * @param {object} data
   * @returns {*}
   */
  function generateTemplate(str, data) {
    var fn = new Function("obj",
      "var p=[],print=function(){p.push.apply(p,arguments);};" +
      "with(obj){p.push('" +
      str
        .replace(/[\r\t\n]/g, " ")
        .split("<%").join("\t")
        .replace(/((^|%>)[^\t]*)'/g, "$1\r")
        .replace(/\t=(.*?)%>/g, "',$1,'")
        .split("\t").join("');")
        .split("%>").join("p.push('")
        .split("\r").join("\\'") + "');}return p.join('');"
    );

    if(!/\W/.test(str)) {
      fn = generateTemplate(document.getElementById(str).innerHTML);
    }

    return data ? fn(data) : fn;
  }
}

module.exports = VideoPlayer;
