var mocha = require('mocha');
var chai = require('chai');
var expect = chai.expect;

describe('Example module', function() {
  it('should make a passing test', function() {
    expect(true).to.be.true;
  });

  it('should have a pending test');

  it('should have a failing test', function() {
    expect(true).to.be.false;
  });
});
