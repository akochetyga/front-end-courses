'use strict';

const express = require('express');
const app = express();
const cacheManifest = require('connect-cache-manifest');

app.use(express.static(__dirname + '/dist/public'));
app.use(cacheManifest({
  manifestPath: '/application.manifest'
}));

app.listen(8080, function () {
  console.log('front-end-courses local app listening on port 8080!');
})
